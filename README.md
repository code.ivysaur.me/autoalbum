# autoalbum

![](https://img.shields.io/badge/written%20in-PHP-blue)

A bulk renaming utility for audio files.

It attempts to find predetermined patterns amongst the file names.

## Usage


```
Usage:
  autoalbum [options] [--] DIRECTORY

Options:
  --prefix STRING       Prepend all output filenames with STRING
  --auto-prefix         Prepend all output filenames with the directory name
  --digits NUM          Number of digits in track numbers (default 0 == auto)
  --extension STRING    Filter by file extension (default 'mp3')
  --dry-run             Display proposed changes, then exit without applying
  --interactive         Interactive prompt to confirm changes (default)
  --accept              Apply changes without confirmation
```


## Alternatives

Renaming audio files is not a new problem. Renaming audio files based solely on filenames is a fraught endeavour. It's almost certainly better to use an ID3 renaming utility. For example `eyeD3 -Q --plugin=fixup --type=lp --dry-run --file-rename-pattern '$album_artist - ($release_date:year) $album - $track:num - $title ($artist)'`.

## Changelog

2017-04-30 v14
- Initial public release
- [⬇️ autoalbum-v14.tar.xz](dist-archive/autoalbum-v14.tar.xz) *(2.64 KiB)*

